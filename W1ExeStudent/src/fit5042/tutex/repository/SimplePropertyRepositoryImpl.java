/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository
{
	private ArrayList<Property> properties;

    public SimplePropertyRepositoryImpl() 
    {
        this.properties = new ArrayList<Property>();
    }
    
    public void addProperty(Property property) throws Exception
    {
    	boolean isAdd = true;
    	for (Property holder : properties) 
    	{
    		if (property.getId() == holder.getId()) 
    		{
    			System.out.println("Invalid propertyID, already exist!");
    			isAdd = false;
    			break;
    		}
    	}
    	if (isAdd == true)
    		this.properties.add(property);
    }
    
    public Property searchPropertyById(int id) throws Exception
    {
    	Property target = null;
    	for (Property property : properties) 
    	{
    		if (property.getId() == id) 
    		{
    			target = property;
    			break;
    		}
    	}
    	return target;
    }
    
    public List<Property> getAllProperties() throws Exception
    {
    	return properties;
    }
}
