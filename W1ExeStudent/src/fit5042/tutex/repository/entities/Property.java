/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository.entities;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 *
 * @author Junyang
 * 
 */
//TODO Exercise 1.3 Step 1 Please refer tutorial exercise. 
public class Property 
{
    private int id;
    private String address;
    private int numberOfBedrooms;
    private int size;
    private double prize;
    
	public Property(int id, String address, int numberOfBedrooms, int size, double prize) 
	{
		super();
		this.id = id;
		this.address = address;
		this.numberOfBedrooms = numberOfBedrooms;
		this.size = size;
		this.prize = prize;
	}
	
	public int getId() 
	{
		return id;
	}
	
	public void setId(int id) 
	{
		this.id = id;
	}
	
	public String getAddress() 
	{
		return address;
	}
	
	public void setAddress(String address) 
	{
		this.address = address;
	}
	
	public int getNumberOfBedrooms() 
	{
		return numberOfBedrooms;
	}
	
	public void setNumberOfBedrooms(int numberOfBedrooms) 
	{
		this.numberOfBedrooms = numberOfBedrooms;
	}
	
	public int getSize() 
	{
		return size;
	}
	
	public void setSize(int size) 
	{
		this.size = size;
	}
	
	public double getPrize() 
	{
		return prize;
	}
	
	public void setPrize(double prize) 
	{
		this.prize = prize;
	}
	
	@Override
	public String toString() 
	{
		return id + " " + address + " " + numberOfBedrooms + "BR(s) "
				+ String.format("%.2f", (float)size) + "sqm $" + String.format("%.2f", prize);
	}
    
}
